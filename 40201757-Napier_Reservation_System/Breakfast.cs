﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Breakfast class consists of all the associated properties required to store the data for a breakfast extra.
 *              It inherits from the MealDecorator class and it allows any created Meals to be decorated as a Breakfast.
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757_Napier_Reservation_System
{
    // The Breakfast class is a Concrete Decorator.
    public class Breakfast : MealDecorator
    {
        private string description;

        // Every Breakfast has price and extra type.
        // If the Meal is of type Breakfast we have to add £4.50 more.
        public Breakfast(Extra mealItem):base(mealItem)
        {
            this.Price = mealItem.Price + 4.5;
            this.ExtraType = "Breakfast";
        }

        // Sets and gets the description for the meal.
        public string Description
        {
            set
            {
                description = value;
            }
            get
            {
                return description;
            }
        }

        // The toCSV() method is used when writing the data to a CSV file.
        public override string toCSV(int refNumber)
        {
            return string.Format("{0},{1},{2},{3}", refNumber, this.ExtraType, this.Description, this.Price);
        }
    }
}
