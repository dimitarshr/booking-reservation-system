﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The CarHire class consists of all the associated properties required to store the data for a car hire extra.
 *              It inherits from the Extra class, which allows us to treat any CarHire objects as an Extra object.
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757_Napier_Reservation_System
{
    class CarHire : Extra
    {
        private string driverName;
        private DateTime startDate;
        private DateTime endDate;

        // Every Car Hire extra requires the driver's name, start and end date.
        public CarHire(string driverName, DateTime startDate, DateTime endDate)
        {
            // The start date can't be greater than or equal to the end date.
            if (startDate >= endDate)
            {
                throw new ArgumentException("Please specify correct dates for the Car Hire extra!");
            }
            else
            {
                this.Price = 50;
                this.ExtraType = "Car Hire";
                this.DriverName = driverName;
                this.StartDate = startDate;
                this.EndDate = endDate;
            }
        }

        // The DriverName property sets and returns the name of the driver.
        public string DriverName
        {
            set
            {
                // The name can't be empty string.
                if (value.Trim() == "")
                {
                    throw new ArgumentException("Please provide the name of the driver!");
                }
                driverName = value;
            }
            get
            {
                return driverName;
            }
        }
        
        // Sets and gets the start date for the extra.
        public DateTime StartDate
        {
            set
            {
                // The start date can't be in the past.
                if (value < DateTime.Today)
                {
                    throw new ArgumentException("Please specify a correct start date for the car hire!");
                }
                startDate = value;
            }
            get
            {
                return startDate;
            }
        }

        // Sets and gets the end date for the exta.
        public DateTime EndDate
        {
            set
            {
                // The end date can't be the current day or in the past.
                if (value <= DateTime.Today)
                {
                    throw new ArgumentException("Please specify a correct end date for the car hire!");
                }
                endDate = value;
            }
            get
            {
                return endDate;
            }
        }

        // Returns the number of day for which the vehicle is hired.
        public double Days
        {
            get
            {
                return (this.EndDate - this.StartDate).TotalDays;
            }
        }

        // The toCSV() method is used when writing the data to a CSV file.
        public override string toCSV(int refNumber)
        {
            return string.Format("{0},{1},{2},{3},{4},{5}", refNumber, this.ExtraType, this.DriverName, this.StartDate.ToShortDateString(), this.EndDate.ToShortDateString(), this.Price);
        }
    }
}
