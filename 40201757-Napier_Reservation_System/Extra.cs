﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Extra class consists of all the associated properties required to store the basic data for an extra.
 *              It is part of the Decorator design pattern and it is the Component class.
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757_Napier_Reservation_System
{
    public abstract class Extra
    {
        private double price;
        private string extraType;

        // The ExtraType is a string defining the extra. 
        // It is used when writing to the CSV file and it clearly shows what is the type of the extra.
        public string ExtraType
        {
            set
            {   // The string can not be empty.
                if (value.Trim() == "")
                {
                    throw new ArgumentException("Please, provide the type of the extra.");
                }
                extraType = value;
            }
            get
            {
                return extraType;
            }
        }

        // Each extra has a price that is included in the bill calculation.
        public double Price
        {
            set
            {   // The price can not be negative number.
                if (value < 0)
                {
                    throw new ArgumentException("Please, provide correct price for the extra.");
                }
                price = value;
            }
            get
            {
                return price;
            }
        }

        // The toCSV() method is abstract as every extra has its own implementation.
        public abstract string toCSV(int refNumber);
    }
}
