﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The EditBooking class is part of the GUI.
 *              In this window bookings can be edited and deleted.
 *              We can add/remove extras and guests.
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40201757_Napier_Reservation_System
{
    public partial class EditBooking : Window
    {

        // Creates private instances of the file pointer and the object containing all the current data (all customers and all bookings).
        private CSVfileSingleton filePointer = CSVfileSingleton.Instance;
        private DataSingleton currentData = DataSingleton.Instance;
        public EditBooking()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // When loaded this sets the default window.
            defaultWindow();
            disableWindow();
            foreach (Booking booking in currentData.AllBookings.Values)
            {
                lbBooking.Items.Add(booking);
            }
        }
        private void lbBooking_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // The selected index is -1 if there is no selected item from the list box.
            if (lbBooking.SelectedIndex != -1)
            {
                enableWindow();

                // Store the selected booking.
                Booking selectedBooking = (Booking)(lbBooking.SelectedItem);

                defaultWindow();
                
                // Load the departure and the arrival dates.
                dpArrival.SelectedDate = selectedBooking.ArrivalDate;
                dpDeparture.SelectedDate = selectedBooking.DepartureDate;

                // Load the guest list for the current booking.
                foreach (Guest guest in selectedBooking.GuestList)
                {
                    lbAllGuests.Items.Add(guest.ToString());
                    currentData.addGuest(guest);
                }

                // Load all the extras for the selected booking.
                foreach (Extra extra in selectedBooking.AllExtra)
                {
                    if (extra.GetType() == typeof(Breakfast))
                    {
                        cbBreakfast.IsChecked = true;
                        tbBreakfast.Text = ((Breakfast)extra).Description;
                    }
                    else if (extra.GetType() == typeof(EveningMeal))
                    {
                        cbEveningMeals.IsChecked = true;
                        tbEveningMeals.Text = ((EveningMeal)extra).Description;
                    }
                    else if (extra.GetType() == typeof(CarHire))
                    {
                        cbCarHire.IsChecked = true;
                        tbDriverName.Text = ((CarHire)extra).DriverName;
                        dpStartDate.SelectedDate = ((CarHire)extra).StartDate;
                        dpEndDate.SelectedDate = ((CarHire)extra).EndDate;
                    }
                }
            }
        }
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            // Clears the temporary list for the extras.
            currentData.TempExtraList.Clear();

            // Store the index of the currently selected booking.
            int selectedIndex = lbBooking.SelectedIndex;

            // Get the selected booking.
            Booking selectedBooking = (Booking)(lbBooking.SelectedItem);
            
            // Get the customer for the selected booking.
            Customer currentCustomer = selectedBooking.Cust;

            // Get the new arrival and departure dates.
            DateTime arrivalDate = Convert.ToDateTime(dpArrival.SelectedDate);
            DateTime departureDate = Convert.ToDateTime(dpDeparture.SelectedDate);

            // If the departure date is after or the same as the arrival date, show an error message.
            if (arrivalDate >= departureDate)
            {
                MessageBox.Show("The arrival/departure dates are incorrect!");
                return;
            }
            
            // If there are no guests for the current booking, show an error message.
            if (currentData.TempGuestList.Count == 0)
            {
                MessageBox.Show("The booking must have at least one guest!");
                return;
            }

            // Checks the window form for any selected extras.
            if (!checkForExtras())
            {
                // If the CarHire object fails, the booking creation process is stopped.
                return;
            }

            // Change the arrival and departure dates of the selected booking with the new values.
            selectedBooking.ArrivalDate = arrivalDate;
            selectedBooking.DepartureDate = departureDate;

            // Store the new extras for the selected booking.
            selectedBooking.AllExtra.Clear();
            selectedBooking.AllExtra = currentData.TempExtraList;
            // Store the new guest list for the selected booking.

            selectedBooking.GuestList.Clear();
            selectedBooking.GuestList = currentData.TempGuestList;

            // Gets the selected booking, the customer, his/her list of bookings and removes the changed booking.
            selectedBooking.Cust.AllBookings.Remove(selectedBooking.BookingRefNumber);

            // Gets the selected booking, the customer, his/her list of bookings and adds the new(edited) booking.
            selectedBooking.Cust.AllBookings.Add(selectedBooking.BookingRefNumber, selectedBooking);

            // Reload the booking list box.
            lbBooking.Items.Clear();
            foreach (Booking booking in currentData.AllBookings.Values)
            {
                lbBooking.Items.Add(booking);
            }
            lbBooking.SelectedIndex = selectedIndex;

            // Rewrite the booking file with the updated records.
            filePointer.writeFileBooking();

            MessageBox.Show("The changes are made successfully!");

            // Clears the temporary list for the extras.
            currentData.TempExtraList.Clear();
        }

        // Checks the window for any selected extras.
        private bool checkForExtras()
        {
            if (cbBreakfast.IsChecked.Value)
            {
                // Creates an instance of Meal.
                Meal extraMeal = new Meal();
                // Decorates the Meal as a Breakfast.
                Breakfast extraBreakfast = new Breakfast(extraMeal);
                extraBreakfast.Description = tbBreakfast.Text;
                // Add the extra to the list.
                currentData.addExtra(extraBreakfast);
            }
            if (cbEveningMeals.IsChecked.Value)
            {
                // Creates an instance of Meal.
                Meal extraMeal = new Meal();
                // Decorates the Meal as an Evening Meal.
                EveningMeal extraEveningMeal = new EveningMeal(extraMeal);
                extraEveningMeal.Description = tbEveningMeals.Text;
                // Add the extra to the list.
                currentData.addExtra(extraEveningMeal);
            }
            if (cbCarHire.IsChecked.Value)
            {
                // Creates a reference of type CarHire and stores the relevant information from the window form.
                CarHire extraCarHire;
                DateTime startDate = Convert.ToDateTime(dpStartDate.SelectedDate);
                DateTime endDate = Convert.ToDateTime(dpEndDate.SelectedDate);

                // If the end date is after or the same as the start date, show an error message.
                if (startDate >= endDate)
                {
                    MessageBox.Show("The start/end dates for the Car Hire are incorrect!");
                    return false;
                }

                // Tries to create an instance of type CarHire
                try
                {
                    extraCarHire = new CarHire(tbDriverName.Text, startDate, endDate);
                }
                catch (Exception except)
                {
                    MessageBox.Show(except.Message);
                    return false;
                }

                // Add the extra to the list.
                currentData.addExtra(extraCarHire);
            }
            // If all extras are created successfully 'true' is returned.
            return true;
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            // Get the selected booking.
            Booking selectedBooking = (Booking)(lbBooking.SelectedItem);
            int customerIndex = selectedBooking.BookingRefNumber;

            // Remove the booking from the customer's list of bookings.
            selectedBooking.Cust.AllBookings.Remove(customerIndex);

            // Remove the booking from the booking dictionary.
            currentData.AllBookings.Remove(selectedBooking.BookingRefNumber);

            // Remove the booking from the list box.
            lbBooking.Items.RemoveAt(lbBooking.SelectedIndex);

            // Rewrite the booking file with the updated records.
            filePointer.writeFileBooking();

            // Set the window back to default.
            defaultWindow();
            disableWindow();
            if (currentData.AllBookings.Count == 0)
            {
                MessageBox.Show("There are no more bookings to be edited!");
            }
        }
        private void btnAddGuest_Click(object sender, RoutedEventArgs e)
        {
            int age;
            Guest currentGuest;

            // If there are already 4 guests for the current booking, show an error.
            if (currentData.TempGuestList.Count == 4)
            {
                MessageBox.Show("There is space for only 4 guest per booking!");
                txtGuestName.Text = "";
                txtGuestPassNumber.Text = "";
                txtGuestAge.Text = "";
                return;
            }

            // Validate the age, which should be an integer.
            if (int.TryParse(txtGuestAge.Text, out age))
            {
                // Try to create an instance of type Guest.
                try
                {
                    currentGuest = new Guest(txtGuestName.Text, txtGuestPassNumber.Text, age);
                }
                catch (Exception except)
                {
                    MessageBox.Show(except.Message);
                    currentGuest = null;
                    return;
                }
                // If the guest is successfully created, add him/her to the list of guests for the current booking.
                currentData.addGuest(currentGuest);

                txtGuestName.Text = "";
                txtGuestPassNumber.Text = "";
                txtGuestAge.Text = "";

                // Add the new guest to the list box.
                lbAllGuests.Items.Add(currentGuest.ToString());
            }
            // If the validation fails, show helpful error message.
            else
            {
                MessageBox.Show("The age value must be an integer!");
            }

        }
        private void btnRemoveGuest_Click(object sender, RoutedEventArgs e)
        {
            int removeIndex = lbAllGuests.SelectedIndex;

            // Remove the selected guest from the guest list.
            currentData.TempGuestList.RemoveAt(removeIndex);

            // Remove the selected guest from the list box.
            lbAllGuests.Items.RemoveAt(removeIndex);
        }
       
        private void btnUpdateGuest_Click(object sender, RoutedEventArgs e)
        {
            int newAge;
            Guest selectedGuest = currentData.TempGuestList[lbAllGuests.SelectedIndex];
            // Validate the data before changing the details of the selected guest.
            if (txtGuestName.Text != "" && txtGuestPassNumber.Text != "" && int.TryParse(txtGuestAge.Text, out newAge))
            {
                selectedGuest.Name = txtGuestName.Text;
                selectedGuest.PassNumber = txtGuestPassNumber.Text;
                selectedGuest.Age = newAge;

                // Reload the list box with all guests.
                lbAllGuests.Items.Clear();
                foreach (Guest guest in currentData.TempGuestList)
                {
                    lbAllGuests.Items.Add(guest.ToString());
                }
            }

            // If the validation fails, show a helpful error message.
            else
            {
                MessageBox.Show("The new information for the guest is incorrect!");
            }
        }

        private void lbAllGuests_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selectedIndex = lbAllGuests.SelectedIndex;
            
            // If there is a selected guest, load all the information about him/her in the window form.
            if (selectedIndex != -1)
            {
                btnRemoveGuest.IsEnabled = true;
                btnUpdateGuest.IsEnabled = true;

                Guest selectedGuest = currentData.TempGuestList[selectedIndex];
                txtGuestName.Text = selectedGuest.Name;
                txtGuestAge.Text = selectedGuest.Age.ToString();
                txtGuestPassNumber.Text = selectedGuest.PassNumber;
            }
            else
            {
                btnUpdateGuest.IsEnabled = false;
                btnRemoveGuest.IsEnabled = false;
                txtGuestName.Text = "";
                txtGuestAge.Text = "";
                txtGuestPassNumber.Text = "";
            }
        }

        private void cbBreakfast_Checked(object sender, RoutedEventArgs e)
        {
            tbBreakfast.IsEnabled = true;
        }

        private void cbEveningMeals_Checked(object sender, RoutedEventArgs e)
        {
            tbEveningMeals.IsEnabled = true;
        }

        private void cbCarHire_Checked(object sender, RoutedEventArgs e)
        {
            tbDriverName.IsEnabled = true;
            dpStartDate.IsEnabled = true;
            dpEndDate.IsEnabled = true;
        }

        private void cbBreakfast_Unchecked(object sender, RoutedEventArgs e)
        {
            tbBreakfast.Text = "";
            tbBreakfast.IsEnabled = false;
        }

        private void cbEveningMeals_Unchecked(object sender, RoutedEventArgs e)
        {
            tbEveningMeals.Text = "";
            tbEveningMeals.IsEnabled = false;
        }

        private void cbCarHire_Unchecked(object sender, RoutedEventArgs e)
        {
            tbDriverName.IsEnabled = false;
            dpStartDate.IsEnabled = false;
            dpEndDate.IsEnabled = false;
            tbDriverName.Text = "";
            dpStartDate.SelectedDate = DateTime.Today;
            dpEndDate.SelectedDate = DateTime.Today.AddDays(1);
        }

        // Used to set the whole window to default condition.
        private void defaultWindow()
        {
            dpArrival.SelectedDate = DateTime.Today;
            dpDeparture.SelectedDate = DateTime.Today.AddDays(1);
            cbBreakfast.IsChecked = false;
            cbEveningMeals.IsChecked = false;
            cbCarHire.IsChecked = false;
            tbBreakfast.Text = "";
            tbEveningMeals.Text = "";
            tbDriverName.Text = "";
            dpStartDate.SelectedDate = DateTime.Today;
            dpEndDate.SelectedDate = DateTime.Today.AddDays(1);
            lbAllGuests.Items.Clear();
            txtGuestName.Text = "";
            txtGuestPassNumber.Text = "";
            txtGuestAge.Text = "";
            currentData.TempGuestList.Clear();
            currentData.TempExtraList.Clear();
        }

        // Used to disable the whole window.
        private void disableWindow()
        {
            dpArrival.IsEnabled = false;
            dpDeparture.IsEnabled = false;
            cbBreakfast.IsEnabled = false;
            cbEveningMeals.IsEnabled = false;
            cbCarHire.IsEnabled = false;
            lbAllGuests.IsEnabled = false;
            txtGuestName.IsEnabled = false;
            txtGuestPassNumber.IsEnabled = false;
            txtGuestAge.IsEnabled = false;
            btnAddGuest.IsEnabled = false;
            btnUpdate.IsEnabled = false;
            btnDelete.IsEnabled = false;
        }

        // Used to enable the whole window.
        private void enableWindow()
        {
            dpArrival.IsEnabled = true;
            dpDeparture.IsEnabled = true;
            cbBreakfast.IsEnabled = true;
            cbEveningMeals.IsEnabled = true;
            cbCarHire.IsEnabled = true;
            lbAllGuests.IsEnabled = true;
            txtGuestName.IsEnabled = true;
            txtGuestPassNumber.IsEnabled = true;
            txtGuestAge.IsEnabled = true;
            btnAddGuest.IsEnabled = true;
            btnUpdate.IsEnabled = true;
            btnDelete.IsEnabled = true;
        }
    }
}
