﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Meal class consists of all the associated properties required to store the basic data for a meal extra.
 *              It inherits from the Extra class, it is part of the Decorator design pattern and it is one of the ConcreteComponent classes.
 *              Any meal can be decorated as either Breakfast or EveningMeal.
 *              This gives the flexibility of creating as many meals as we want and decorating them based on the requirements from the user.
 *              When decorating we are adding the necessary price to the price of a basic meal which is £0.50
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757_Napier_Reservation_System
{
    public class Meal : Extra
    {
        // Every Meal has basic extra type and price.
        public Meal()
        {
            this.ExtraType = "Basic Meal";
            this.Price = 0.5;
        }

        // The toCSV() method is used when writing the data to a CSV file.
        public override string toCSV(int refNumber)
        {
            return string.Format("{0},{1},{2},{3}", refNumber, this.ExtraType, this.Price);
        }
    }
}
