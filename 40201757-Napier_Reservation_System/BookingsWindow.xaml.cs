﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The BookingWindow class is part of the GUI.
 *              In this window bookings can be created, with extras and guests.
 *              In order to create a booking there must be some registered customers.
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40201757_Napier_Reservation_System
{
    public partial class BookingsWindow : Window
    {
        // Creates private instances of the file pointer and the object containing all the current data (all customers and all bookings).
        private CSVfileSingleton filePointer = CSVfileSingleton.Instance;
        private DataSingleton currentData = DataSingleton.Instance;

        public BookingsWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // When loaded this sets the default window.
            currentData.TempExtraList.Clear();
            currentData.TempGuestList.Clear();
            lbCust.SelectedIndex = 0;
            dpArrival.SelectedDate = DateTime.Today;
            dpDeparture.SelectedDate = DateTime.Today.AddDays(1);
            dpStartDate.SelectedDate = DateTime.Today;
            dpEndDate.SelectedDate = DateTime.Today.AddDays(1);
            foreach (Customer cust in currentData.AllCustomers.Values)
            {
                lbCust.Items.Add(cust);
            }
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            // Clears the temporary list for the extras.
            currentData.TempExtraList.Clear();

            // The selected index is -1 if there is no selected item from the list box.
            if (lbCust.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a customer!");
            }
            else
            {
                // Retrieves the necessary data from the windows form.
                Customer currentCustomer = (Customer)(lbCust.SelectedItem);
                DateTime arrivalDate = Convert.ToDateTime(dpArrival.SelectedDate);
                DateTime departureDate = Convert.ToDateTime(dpDeparture.SelectedDate);

                // If the departure date is after or the same as the arrival date, show an error message.
                if (arrivalDate >= departureDate)
                {
                    MessageBox.Show("The arrival/departure dates are incorrect!");
                    return;
                }

                // If there are no guests for the current booking, show an error message.
                if (currentData.TempGuestList.Count == 0)
                {
                    MessageBox.Show("You must add at least one guest!");
                    return;
                }

                // Checks the window form for any selected extras.
                if (!checkForExtras())
                {
                    // If the CarHire object fails, the booking creation process is stopped.
                    return;
                }

                // Create a refetence of type Booking and try to create an instance of type Booking.
                Booking newBooking;
                try
                {
                    newBooking = new Booking(arrivalDate, departureDate, currentCustomer);
                }
                catch (Exception except)
                {
                    MessageBox.Show(except.Message);
                    newBooking = null;
                    return;
                }

                // Store all extras and guests for the current booking.
                newBooking.AllExtra = currentData.TempExtraList;
                newBooking.GuestList = currentData.TempGuestList;
                currentCustomer.addBooking(newBooking);
                currentData.addBooking(newBooking);

                // Write the new booking back to the file.
                filePointer.writeSingleBooking(newBooking);

                MessageBox.Show("The booking is successfully made!");
                defaultWindow();
            }
        }

        // Checks the window for any selected extras.
        private bool checkForExtras()
        {
            if (cbBreakfast.IsChecked.Value)
            {
                // Creates an instance of Meal.
                Meal extraMeal = new Meal();
                // Decorates the Meal as a Breakfast.
                Breakfast extraBreakfast = new Breakfast(extraMeal);
                extraBreakfast.Description = tbBreakfast.Text;
                // Add the extra to the list.
                currentData.addExtra(extraBreakfast);
            }
            if (cbEveningMeals.IsChecked.Value)
            {
                // Creates an instance of Meal.
                Meal extraMeal = new Meal();
                // Decorates the Meal as an Evening Meal.
                EveningMeal extraEveningMeal = new EveningMeal(extraMeal);
                extraEveningMeal.Description = tbEveningMeals.Text;
                // Add the extra to the list.
                currentData.addExtra(extraEveningMeal);
            }
            if (cbCarHire.IsChecked.Value)
            {
                // Creates a reference of type CarHire and stores the relevant information from the window form.
                CarHire extraCarHire;
                DateTime startDate = Convert.ToDateTime(dpStartDate.SelectedDate);
                DateTime endDate = Convert.ToDateTime(dpEndDate.SelectedDate);
                
                // If the end date is after or the same as the start date, show an error message.
                if (startDate >= endDate)
                {
                    MessageBox.Show("The start/end dates for the Car Hire are incorrect!");
                    return false;
                }
                
                // Tries to create an instance of type CarHire
                try
                {
                    extraCarHire = new CarHire(tbDriverName.Text, startDate, endDate);
                }
                catch (Exception except)
                {
                    MessageBox.Show(except.Message);
                    return false;
                }
                // Add the extra to the list.
                currentData.addExtra(extraCarHire);
            }
            // If all extras are created successfully 'true' is returned.
            return true;
        }
        
        private void btnAddGuest_Click(object sender, RoutedEventArgs e)
        {
            int age;
            Guest currentGuest;

            // If there are already 4 guests for the current booking, show an error.
            if (currentData.TempGuestList.Count == 4)
            {
                MessageBox.Show("There is space for only 4 guest per booking!");
                txtGuestName.Text = "";
                txtGuestPassNumber.Text = "";
                txtGuestAge.Text = "";
                return;
            }
            
            // Validate the age, which should be an integer.
            if (int.TryParse(txtGuestAge.Text, out age))
            {
                // Try to create an instance of type Guest.
                try
                {
                    currentGuest = new Guest(txtGuestName.Text, txtGuestPassNumber.Text, age);
                }
                catch (Exception except)
                {
                    MessageBox.Show(except.Message);
                    currentGuest = null;
                    return;
                }
                // If the guest is successfully created, add him/her to the list of guests for the current booking.
                currentData.addGuest(currentGuest);

                txtGuestName.Text = "";
                txtGuestPassNumber.Text = "";
                txtGuestAge.Text = "";

                // Add the new guest to the list box.
                lbAllGuests.Items.Add(currentGuest.ToString());

                MessageBox.Show("Guest successfully created!");
            }
            
            // If the validation fails, show helpful error message.
            else
            {
                MessageBox.Show("The age value must be an integer!");
            }
        }

        private void btnRemoveGuest_Click(object sender, RoutedEventArgs e)
        {
            int removeIndex = lbAllGuests.SelectedIndex;
            // Remove the selected guest from the guest list.
            currentData.TempGuestList.RemoveAt(removeIndex);
            // Remove the selected guest from the list box.
            lbAllGuests.Items.RemoveAt(removeIndex);
        }

        private void btnUpdateGuest_Click(object sender, RoutedEventArgs e)
        {
            int newAge;
            Guest selectedGuest = currentData.TempGuestList[lbAllGuests.SelectedIndex];
            // Validate the data before changing the details of the selected guest.
            if (txtGuestName.Text != "" && txtGuestPassNumber.Text != "" && int.TryParse(txtGuestAge.Text, out newAge))
            {
                selectedGuest.Name = txtGuestName.Text;
                selectedGuest.PassNumber = txtGuestPassNumber.Text;
                selectedGuest.Age = newAge;

                // Reload the list box with all guests.
                lbAllGuests.Items.Clear();
                foreach (Guest guest in currentData.TempGuestList)
                {
                    lbAllGuests.Items.Add(guest.ToString());
                }
            }

            // If the validation fails, show a helpful error message.
            else
            {
                MessageBox.Show("The new information for the guest is incorrect!");
            }
        }

        private void lbAllGuests_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selectedIndex = lbAllGuests.SelectedIndex;

            // If there is a selected guest, load all the information about him/her in the window form.
            if (selectedIndex != -1)
            {
                btnRemoveGuest.IsEnabled = true;
                btnUpdateGuest.IsEnabled = true;

                Guest selectedGuest = currentData.TempGuestList[selectedIndex];
                txtGuestName.Text = selectedGuest.Name;
                txtGuestAge.Text = selectedGuest.Age.ToString();
                txtGuestPassNumber.Text = selectedGuest.PassNumber;
            }
            else
            {
                btnUpdateGuest.IsEnabled = false;
                btnRemoveGuest.IsEnabled = false;
                txtGuestName.Text = "";
                txtGuestAge.Text = "";
                txtGuestPassNumber.Text = "";
            }
        }
        private void cbBreakfast_Checked(object sender, RoutedEventArgs e)
        {
            tbBreakfast.IsEnabled = true;
        }

        private void cbEveningMeals_Checked(object sender, RoutedEventArgs e)
        {
            tbEveningMeals.IsEnabled = true;
        }

        private void cbCarHire_Checked(object sender, RoutedEventArgs e)
        {
            tbDriverName.IsEnabled = true;
            dpStartDate.IsEnabled = true;
            dpEndDate.IsEnabled = true;
        }

        private void cbBreakfast_Unchecked(object sender, RoutedEventArgs e)
        {
            tbBreakfast.Text = "";
            tbBreakfast.IsEnabled = false;
        }

        private void cbEveningMeals_Unchecked(object sender, RoutedEventArgs e)
        {
            tbEveningMeals.Text = "";
            tbEveningMeals.IsEnabled = false;
        }

        private void cbCarHire_Unchecked(object sender, RoutedEventArgs e)
        {
            tbDriverName.IsEnabled = false;
            dpStartDate.IsEnabled = false;
            dpEndDate.IsEnabled = false;
            tbDriverName.Text = "";
            dpStartDate.SelectedDate = DateTime.Today;
            dpEndDate.SelectedDate = DateTime.Today.AddDays(1);
        }

        // Used to set the whole window to default condition.
        private void defaultWindow()
        {
            dpArrival.SelectedDate = DateTime.Today;
            dpDeparture.SelectedDate = DateTime.Today.AddDays(1);
            cbBreakfast.IsChecked = false;
            cbEveningMeals.IsChecked = false;
            cbCarHire.IsChecked = false;
            tbBreakfast.Text = "";
            tbEveningMeals.Text = "";
            tbDriverName.Text = "";
            dpStartDate.SelectedDate = DateTime.Today;
            dpEndDate.SelectedDate = DateTime.Today.AddDays(1);
            lbAllGuests.Items.Clear();
            txtGuestName.Text = "";
            txtGuestPassNumber.Text = "";
            txtGuestAge.Text = "";
            currentData.TempGuestList.Clear();
            currentData.TempExtraList.Clear();
        }

        private void lbCust_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            defaultWindow();
        }
    }
}