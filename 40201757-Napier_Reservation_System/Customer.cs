﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Customer class consists of all the associated properties required to store the data for a Customer.
 *              It inherits all the attributes and properties from the Person class. 
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757_Napier_Reservation_System
{
    public class Customer:Person
    {
        private string address;
        private int customerRefNumber;
        static private int customersCounter = 0;

        private Dictionary<int, Booking> allBookings = new Dictionary<int, Booking>();

        // Every customer must have a name and an address.
        public Customer(string name, string address)
        {
            this.Name = name;
            this.Address = address;
            customersCounter += 1;
            customerRefNumber = customersCounter;
        }

        // Creates a customer object that has ID.
        public Customer(int id, string name, string address)
        {
            this.customerRefNumber = id;
            this.Name = name;
            this.Address = address;
            Customer.customersCounter = id;
        }

        // The Address property sets and returns the address of the customer.
        public string Address
        {
            set
            {   // The address can not be empty string.
                if (value.Trim() == "")
                {
                    throw new ArgumentException("Please provide value for address.");
                }
                address = value;
            }
            get
            {
                return address;
            }
        }

        // Returns the Customer Reference Number
        public int CustomerRefNumber
        {
            get
            {
                return customerRefNumber;
            }
        }

        // Returns a dictionary with all bookings belonging to this customer.
        public Dictionary<int,Booking> AllBookings
        {
            get
            {
                return allBookings;
            }
        }

        // Adds a single booking to the dictionary.
        public void addBooking(Booking newbooking)
        {
            allBookings.Add(newbooking.BookingRefNumber, newbooking);
        }

        // Overriding the ToString() method.
        public override string ToString()
        {
            return this.CustomerRefNumber.ToString() + "\n" + this.Name +", "+ this.Address;
        }

        // Returns string format used for writing to a CSV file.
        public string toCSV()
        {
            return string.Format("{0},{1},{2}", this.CustomerRefNumber, this.Name, this.Address);
        }
    }
}
