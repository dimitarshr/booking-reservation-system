﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The MainWindow class is part of the GUI.
 *              This is the starting window.
 *              From here we can go to the CustomersWindow, BookingWindow, EditBookingWindow and the Invoice window.
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _40201757_Napier_Reservation_System
{
    public partial class MainWindow : Window
    {
        // Creates private instances of the file pointer and the object containing all the current data (all customers and all bookings).
        private CSVfileSingleton filePointer = CSVfileSingleton.Instance;
        private DataSingleton currentData = DataSingleton.Instance;

        public MainWindow()
        {
            InitializeComponent();
        }

        // Button for the customer window.
        private void btnCustomers_Click(object sender, RoutedEventArgs e)
        {
            // Load a window for adding and editing customers.
            CustomersWindow customerWindow = new CustomersWindow();
            customerWindow.ShowDialog();
        }

        // Button for the bookings window.
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Check if there are any customers to create bookings.
            if (currentData.AllCustomers.Count == 0)
            {
                MessageBox.Show("Please register a customer first!");
            }
            else
            {
                // Load a window for creating new bookings.
                BookingsWindow bookingWindow = new BookingsWindow();
                bookingWindow.ShowDialog();
            }
        }

        private void btnEditBooking_Click(object sender, RoutedEventArgs e)
        {
            // Check if there are any bookings to be edited.
            if (currentData.AllBookings.Count == 0)
            {
                MessageBox.Show("There are no existing bookings!");
            }
            else
            {
                // Load a window for editing windows.
                EditBooking bookingWindow = new EditBooking();
                bookingWindow.ShowDialog();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Load all existing customers.
            filePointer.readFileCustomers();
            
            // Load all existing bookings.
            filePointer.readFileBookings();
        }

        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            // Check if there are any bookings.
            if (currentData.AllBookings.Count == 0)
            {
                MessageBox.Show("There are no existing bookings!");
            }
            else
            {
                // Load a window to display invoices.
                Invoice bill = new Invoice();
                bill.ShowDialog();
            }

        }
    }
}
