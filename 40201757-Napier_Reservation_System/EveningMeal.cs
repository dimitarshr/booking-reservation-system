﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The EveningMeal class consists of all the associated properties required to store the data for a evening meal extra.
 *              It inherits from the MealDecorator class and it allows any created Meals to be decorated as a EveningMeal.
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757_Napier_Reservation_System
{
    // The EveningMeal class is a Concrete Decorator.
    public class EveningMeal : MealDecorator
    {
        private string description;

        // Every EveningMeal has price and extra type.
        // If the Meal is of type EveningMeal we have to add £14.50 more.
        public EveningMeal(Extra mealItem):base(mealItem)
        {
            this.Price = mealItem.Price + 14.5;
            this.ExtraType = "Evening Meal";
        }
        
        // Sets and gets the description for the meal.
        public string Description
        {
            set
            {
                description = value;
            }
            get
            {
                return description;
            }
        }

        // The toCSV() method is used when writing the data to a CSV file.
        public override string toCSV(int refNumber)
        {
            return string.Format("{0},{1},{2},{3}", refNumber, this.ExtraType, this.Description,this.Price);
        }
    }
}
