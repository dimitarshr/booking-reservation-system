﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Invoice class is part of the GUI.
 *              In this window invoices can be printed for selected booking.
 * Date last modified: 06/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40201757_Napier_Reservation_System
{
    public partial class Invoice : Window
    {   
        // Creates private instances of an object containing all the current data.
        DataSingleton currentData = DataSingleton.Instance;
        public Invoice()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Load all exisiting bookings.
            foreach (Booking booking in currentData.AllBookings.Values)
            {
                lbBookings.Items.Add(booking);
            }
        }

        private void lbBookings_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            labelBooking.Content = "-\t-";
            labelBreakfast.Content = "-\t   -";
            labelEveningMeal.Content = "-\t   -";
            labelCarHire.Content = "-\t   -"; 

            // Get the selected booking.
            Booking selectedBooking = (Booking)(lbBookings.SelectedItem); 
            // Print the booking cost per night.
            labelBooking.Content = selectedBooking.toStringInvoice() + "\t   " + "£" + selectedBooking.billBookingPerNight().ToString();
            // For each extra prints the cost per night.
            foreach (Extra extra in selectedBooking.AllExtra) 
            {
                if (extra.GetType() == typeof(Breakfast))
                {
                    labelBreakfast.Content = selectedBooking.toStringInvoice() + "\t   " + "£" + extra.Price*selectedBooking.GuestList.Count;
                }
                else if (extra.GetType() == typeof(EveningMeal))
                {
                    labelEveningMeal.Content = selectedBooking.toStringInvoice() + "\t   " + "£" + extra.Price*selectedBooking.GuestList.Count;
                }
                else
                {
                    labelCarHire.Content = ((CarHire)extra).Days + "\t   " + "£" + extra.Price;
                }
            }
            // Shows the total, extras plus booking.
            tbTotal.Text = "£" + selectedBooking.totalBillBooking().ToString();
        }
    }
}
