﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Guest class consists of all the associated properties required to store the data for a Guest.
 *              It inherits all the attributes and properties from the Person class. 
 * Date last modified: 08/12/2016
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757_Napier_Reservation_System
{
    public class Guest:Person
    {
        private string passNumber;
        private int age;

        // Every guest must have a name, a passport number and an age.
        public Guest(string name, string passNumber, int age)
        {
            this.Name = name;
            this.PassNumber = passNumber;
            this.Age = age;
        }

        // The Age property sets and returns the age of the guest.
        public int Age
        {
            set
            {
                // The guest's age must be between 0 and 101.
                if (value < 0 || value > 101)
                {
                    throw new ArgumentException("The age value must be between 0 and 101.");
                }
                age = value;
            }
            get
            {
                return age;
            }
        }

        // The PassNumber property sets and returns the passport number (passNumber) of the guest.
        public string PassNumber
        {
            set
            {
                if (value.Trim().Length > 10 || value.Trim().Length == 0)
                {
                    throw new ArgumentException("The passport number must be between 1 and 10 characters long!");
                }
                passNumber = value;
            }
            get
            {
                return passNumber;
            }
        }

        // Overriding the ToString() method.
        public override string ToString()
        {
            return "Name: "+this.Name + "\n" + "ID: " +this.passNumber + "\n" + "Age: " +this.Age;
        }

        // Returns string format used for writing to a CSV file.
        public string toCSV(int refNumber)
        {
            return string.Format("{0},{1},{2},{3}", refNumber, this.Name, this.PassNumber, this.Age);
        }
    }
}
