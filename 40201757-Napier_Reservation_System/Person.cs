﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Person class consists of all the associated properties required to store the data for a Person.
 *              It is inherited by Guest and Customer classes. 
 * Date last modified: 06/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757_Napier_Reservation_System
{
    public abstract class Person
    {
        private string name;

        // The Name property sets and returns the name of the person.
        public string Name
        {
            set
            {   // The name can not be empty string.
                if (value.Trim() == "")
                {
                    throw new ArgumentException("Please provide value for name.");
                }
                name = value;
            }
            get
            {
                return name;
            }
        }
    }
}
