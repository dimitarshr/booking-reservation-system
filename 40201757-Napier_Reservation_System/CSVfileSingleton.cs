﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The CSVfileSingleton class consists of all the associated methods required to store the data for a CSV file.
 *              The design pattern used here is Singleton. This allows us to have the same file pointer in different windows. 
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _40201757_Napier_Reservation_System
{
    public class CSVfileSingleton
    {
        private static CSVfileSingleton instance;
        private DataSingleton currentData = DataSingleton.Instance;
        private CSVfileSingleton() { }
        public static CSVfileSingleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CSVfileSingleton();
                }
                return instance;
            }
        }

        // Writes a single record to the customers file.
        public void writeSingleCustomer(Customer customer)
        {
            // The 'true' argument means to append the record.
            StreamWriter writer = new StreamWriter("customers.csv",true);
            writer.WriteLine(customer.toCSV());
            writer.Close();
        }

        // Writes all customer records in the customers file.
        public void writeFileCustomer()
        {
            StreamWriter writer = new StreamWriter("customers.csv");
            foreach (Customer customer in currentData.AllCustomers.Values)
            {
                writer.WriteLine(customer.toCSV());
            }
            writer.Close();
        }

        // Writes a single booking record.
        public void writeSingleBooking(Booking booking)
        {
            // In order to normalise the data, the different records are stored in different files.
            // The 'true' argument means that the records are appended to the files.
            StreamWriter guestWriter = new StreamWriter("guests.csv", true);
            StreamWriter extraWriter = new StreamWriter("extras.csv", true);
            StreamWriter bookingWriter = new StreamWriter("bookings.csv", true);
            bookingWriter.WriteLine(booking.toCSV());
            foreach (Guest guest in booking.GuestList)
            {
                guestWriter.WriteLine(guest.toCSV(booking.BookingRefNumber));
            }
            foreach (Extra extra in booking.AllExtra)
            {
                extraWriter.WriteLine(extra.toCSV(booking.BookingRefNumber));
            }
            bookingWriter.Close();
            guestWriter.Close();
            extraWriter.Close();
        }

        // Writes all booking records in the bookings file.
        public void writeFileBooking()
        {
            // In order to normalise the data, the different records are stored in different files.
            StreamWriter guestWriter = new StreamWriter("guests.csv");
            StreamWriter extraWriter = new StreamWriter("extras.csv");
            StreamWriter bookingWriter = new StreamWriter("bookings.csv");
            foreach (Booking booking in currentData.AllBookings.Values)
            {
                bookingWriter.WriteLine(booking.toCSV());
                foreach (Guest guest in booking.GuestList)
                {
                    guestWriter.WriteLine(guest.toCSV(booking.BookingRefNumber));
                }
                foreach (Extra extra in booking.AllExtra)
                {
                    extraWriter.WriteLine(extra.toCSV(booking.BookingRefNumber));
                }
            }
            bookingWriter.Close();
            guestWriter.Close();
            extraWriter.Close();
        }

        // Reads the whole file for the customers and recreates any customers that already exist.
        public void readFileCustomers()
        {
            // If the file exists, it is read.
            if (File.Exists("customers.csv"))
            {
                StreamReader customerReader = new StreamReader("customers.csv");
                string line;
                while ((line = customerReader.ReadLine()) != null)
                {
                    var values = line.Split(',');
                    // All the existing customers are recreated.
                    Customer customer = new Customer(int.Parse(values[0]), values[1], values[2]);
                    currentData.addCustomer(customer);
                }
                customerReader.Close();
            }
        }

        // Reads the whole file for the bookings and recreates any bookings that already exist.
        public void readFileBookings()
        {
            // If the file exists, it is read.
            if (File.Exists("bookings.csv"))
            {
                StreamReader bookingReader = new StreamReader("bookings.csv");
                string bookingLine;

                while ((bookingLine = bookingReader.ReadLine()) != null)
                {
                    var values = bookingLine.Split(',');
                    // The customer Reference Number is used as a Foreign Key in the guests and extras files.
                    int customerNumber = int.Parse(values[1]);
                    Customer customer = currentData.AllCustomers[customerNumber];
                    Booking booking = new Booking(int.Parse(values[0]), DateTime.Parse(values[3]), DateTime.Parse(values[4]), customer);
                    // These private methods read the guests and extras files
                    // in order to retrieve the information from there about the current booking/customer
                    booking.GuestList = readFileGuests(booking.BookingRefNumber);
                    currentData.TempGuestList.Clear();
                    booking.AllExtra = readFileExtras(booking.BookingRefNumber);
                    currentData.TempExtraList.Clear();
                    currentData.addBooking(booking);
                    customer.addBooking(booking);
                }
                bookingReader.Close();
            }
        }

        // Reads the whole file for the guests associated with this booking.
        private List<Guest> readFileGuests(int bookingNumber)
        {
            if (File.Exists("guests.csv"))
            {
                StreamReader guestReader = new StreamReader("guests.csv");
                string guestLine;
                while ((guestLine = guestReader.ReadLine()) != null)
                {
                    var values = guestLine.Split(',');
                    if (int.Parse(values[0]) == bookingNumber)
                    {   // All guest objects that already exist are recreated.
                        Guest guest = new Guest(values[1], values[2], int.Parse(values[3]));
                        currentData.addGuest(guest);
                    }
                }
                guestReader.Close();
            }
            return currentData.TempGuestList;
        }

        // Reads the whole file for the extras associated with this booking.
        private List<Extra> readFileExtras(int bookingNumber)
        {
            if (File.Exists("extras.csv"))
            {
                StreamReader extraReader = new StreamReader("extras.csv");
                string extraLine;
                while ((extraLine = extraReader.ReadLine()) != null)
                {
                    var values = extraLine.Split(',');
                    if (int.Parse(values[0]) == bookingNumber)
                    {
                        // All extra objects that already exist are recreated.
                        if (values[1] == "Car Hire")
                        {
                            CarHire carhireExtra = new CarHire(values[2], DateTime.Parse(values[3]), DateTime.Parse(values[4]));
                            currentData.addExtra(carhireExtra);
                        }
                        else
                        {
                            Meal extraMeal = new Meal();
                            if (values[1] == "Breakfast")
                            {
                                Breakfast breakfastExtra = new Breakfast(extraMeal);
                                breakfastExtra.Description = values[2];
                                currentData.addExtra(breakfastExtra);
                            }
                            else if (values[1] == "Evening Meal")
                            {
                                EveningMeal eveningmealExtra = new EveningMeal(extraMeal);
                                eveningmealExtra.Description = values[2];
                                currentData.addExtra(eveningmealExtra);
                            }
                        }
                    }
                }
                extraReader.Close();
            }
            return currentData.TempExtraList;
        }
    }
}
