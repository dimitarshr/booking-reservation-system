﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Booking class consists of all the associated properties required to store the data for a booking.
 *              Every booking has a bookingID, which is auto incrementing variable, a customer, list of extras and a list of guests.
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757_Napier_Reservation_System
{
    public class Booking
    {
        private DateTime arrivalDate;
        private DateTime departureDate;
        private int bookingRefNumber;
        static private int bookingsCounter = 0;

        private Customer cust;
        private List<Guest> guestList = new List<Guest>();
        private List<Extra> allExtras = new List<Extra>();

        // Each booking must have arrival date, departure date and a customer.
        public Booking(DateTime arrivalDate, DateTime departureDate, Customer cust)
        {
            // A booking with arrival date after the departure date is invalid.
            if (arrivalDate >= departureDate)
            {
                throw new ArgumentException("Please specify correct arrival/departure dates!");
            }
            else
            {
                this.ArrivalDate = arrivalDate;
                this.DepartureDate = departureDate;
                this.cust = cust;
                bookingsCounter += 1;
                bookingRefNumber = bookingsCounter;
            }
        }

        // Recreates a booking which has already been created and has an ID.
        public Booking(int id, DateTime arrivalDate, DateTime departureDate, Customer cust)
        {
            this.bookingRefNumber = id;
            this.ArrivalDate = arrivalDate;
            this.DepartureDate = departureDate;
            this.cust = cust;
            Booking.bookingsCounter = id;
        }

        // The ArrivalDate property sets and returns the value of the arrival date for the booking.
        public DateTime ArrivalDate
        {
            set
            {   // The arrival date can not be in the past.
                if (value < DateTime.Today)
                {
                    throw new ArgumentException("Please specify a correct arrival date!");
                }
                arrivalDate = value;
            }
            get
            {
                return arrivalDate;
            }
        }

        // The DepartureDate property sets and returns the value of departure date for the booking.
        public DateTime DepartureDate
        {
            set
            {   // The departure date should be in the future.
                if (value <= DateTime.Today)
                {
                    throw new ArgumentException("Please specify a correct departure date!");
                }
                departureDate = value;
            }
            get
            {
                return departureDate;
            }
        }

        // BookingRefNumber returns the reference number of the booking.
        public int BookingRefNumber
        {
            get
            {
                return bookingRefNumber;
            }
        }
        
        // The Cust property returns the customers associated with this booking.
        public Customer Cust
        {
            get
            {
                return cust;
            }
        }

        // Sets and returns a list of all guests for this booking.
        public List<Guest> GuestList
        {
            set
            {
                // A booking can have between 1 and 4 guests.
                if (value.Count > 4 || value.Count == 0)
                {
                    throw new ArgumentException("A booking can not have more than 4 guests in total.");
                }
                foreach (Guest guest in value)
                {
                    guestList.Add(guest);
                }
            }
            get
            {
                return guestList;
            }
        }

        // Sets and returns a list of all extras for this booking.
        public List<Extra> AllExtra
        {
            set
            {
                foreach (Extra extra in value)
                {
                    allExtras.Add(extra);
                }
            }
            get
            {
                return allExtras;
            }
        }

        // The cost of a booking per night.
        public double billBookingPerNight()
        {
            double total = 0;

            foreach (Guest guest in GuestList)
            {
                // If the guest is under 18, they have to pay £30.
                if (guest.Age < 18)
                {
                    total += 30;
                }
                // Otherwise, they have to pay £50.
                else
                {
                    total += 50;
                }
            }
            return total;
        }
       
        // The total cost of a booking including extras.
        public double totalBillBooking()
        {
            double total = 0;
            double nights = (this.DepartureDate - this.ArrivalDate).TotalDays;

            total = this.billBookingPerNight() * nights;
            
            foreach (Extra extra in allExtras)
            {
                // If the extra is Breakfast or Evening Meal, it is calculated based on the number of nights
                // and number of guests
                if (extra.GetType() != typeof(CarHire))
                {
                    total += (nights * extra.Price) * guestList.Count;
                }
                // If it is Car Hire it is calculated based on the number of days.
                else
                {
                    total += ((CarHire)extra).Days * extra.Price;
                }
            }
            return total;
        }

        // Overriding the ToString() method.
        public override string ToString()
        {
            return this.BookingRefNumber.ToString()+" "+this.cust.Name.ToString() + "(Ref Number: "+this.Cust.CustomerRefNumber.ToString()+")"+ "\n" + "From: " + this.ArrivalDate.Date.ToShortDateString() + ", To: " + this.DepartureDate.Date.ToShortDateString();
        }

        // Returns string format used for writing to a CSV file.
        public string toCSV()
        {
            return string.Format("{0},{1},{2},{3},{4}", this.BookingRefNumber, this.Cust.CustomerRefNumber, this.Cust.Name, this.ArrivalDate.Date.ToShortDateString(), this.DepartureDate.Date.ToShortDateString());
        }

        // Returns string format used in the invoice.
        public string toStringInvoice()
        {
            return (this.DepartureDate - this.ArrivalDate).TotalDays.ToString();
        }
    }
}
