﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The CustomerWindow class is part of the GUI.
 *              In this window customers can be created, updated and deleted.
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40201757_Napier_Reservation_System
{
    public partial class CustomersWindow : Window
    {
        // Creates private instances of the file pointer and the object containing all the current data (all bookings and all customers).
        private CSVfileSingleton filePointer = CSVfileSingleton.Instance;
        private DataSingleton currentData = DataSingleton.Instance;
        public CustomersWindow()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            // Create a reference of type Customer.
            Customer customer;

            // Try to create an instance of type Customer.
            try
            {
                customer = new Customer(txtName.Text, txtAddress.Text);
            }
            catch (Exception except)
            {
                MessageBox.Show(except.Message);
                customer = null;
                return;
            }

            // Add the new customer to the currentData dictionary.
            currentData.addCustomer(customer);

            // Add the new customer to the list box.
            lbAllCust.Items.Add(customer);
            txtName.Text = "";
            txtAddress.Text = "";

            // Write the new customer to the file.
            filePointer.writeSingleCustomer(customer);
            MessageBox.Show("Registration successful!");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // On load all customers are loaded into the list box.
            foreach (Customer customer in currentData.AllCustomers.Values)
            {
                lbAllCust.Items.Add(customer);
            }
        }

        private void btnDeleteCust_Click(object sender, RoutedEventArgs e)
        {
            if (lbAllCust.SelectedIndex == -1)
            {
                MessageBox.Show("You have't selected a customer to be deleted!");
            }
            else
            {
                // Get the selected customer from the list box.
                Customer selectedCustomer = (Customer)(lbAllCust.SelectedItem);
                int selectedCustNumber = selectedCustomer.CustomerRefNumber;

                // Check if the customer has any bookings.
                if (currentData.AllCustomers[selectedCustNumber].AllBookings.Count == 0)
                {
                    // Remove the customer from the customer dictionary.
                    currentData.AllCustomers.Remove(selectedCustNumber);
                    // Remove the guest from the list box.
                    lbAllCust.Items.Remove(selectedCustomer);
                    // Rewrite all customers back to the file.
                    filePointer.writeFileCustomer();
                    txtName.Text = "";
                    txtAddress.Text = "";
                }
                // If the customer has any bookings, display a helpful error message.
                else
                {
                    MessageBox.Show("This person has bookings.\nYou can NOT delete a customer with bookings!");
                }
            }
        }

        private void btnUpdateCustomer_Click(object sender, RoutedEventArgs e)
        {
            // Check if there is a selected customer.
            if (lbAllCust.SelectedIndex != -1)
            {
                Customer selectedCustomer = (Customer)(lbAllCust.SelectedItem);
                // Validate the new data for the customer.
                if (txtName.Text != "" && txtAddress.Text != "")
                {
                    // Set the new data.
                    selectedCustomer.Name = txtName.Text;
                    selectedCustomer.Address = txtAddress.Text;
                    
                    // Reload the customer list box with all customers.
                    lbAllCust.Items.Clear();
                    foreach (Customer customer in currentData.AllCustomers.Values)
                    {
                        lbAllCust.Items.Add(customer);
                    }
                    // Rewrite all customers back to the file.
                    filePointer.writeFileCustomer();
                    txtName.Text = "";
                    txtAddress.Text = "";
                    MessageBox.Show("The data is successfully updated.");
                }

                // If the validation fails, display a helpful error message.
                else
                {
                    MessageBox.Show("The new information about the customer is incorrect.");
                }
            }

            // If there is no selected customer, display a helpful error message.
            else
            {
                MessageBox.Show("Please, select a customer.");
            }
        }

        private void lbAllCust_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbAllCust.SelectedIndex != -1)
            {
                // Display the relevant information for the selected guest.
                Customer selectedCustomer = (Customer)(lbAllCust.SelectedItem);
                txtName.Text = selectedCustomer.Name;
                txtAddress.Text = selectedCustomer.Address;
            }
        }
    }
}
