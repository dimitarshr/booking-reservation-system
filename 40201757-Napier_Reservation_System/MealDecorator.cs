﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The MealDecorator class is the Decodator part of the Decorator Design Pattern.
 *              It inherits from the Extra class and it allows any created Meals to be decorated as either a Breakfast or EveningMeal.
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757_Napier_Reservation_System
{
    // The decorator inherits everything from the Extra (Component) class.
    public abstract class MealDecorator : Extra
    {
        protected Extra mealItem;

        public MealDecorator(Extra mealItem)
        {
            this.mealItem = mealItem;
        }

        // The toCSV() method is used when writing the data to a CSV file.
        public override string toCSV(int refNumber)
        {
            return mealItem.toCSV(refNumber);
        }
    }
}
