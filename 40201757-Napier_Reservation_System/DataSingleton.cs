﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The DataSigleton class consists of all the associated properties required to store the data for all customers, bookings, extras and guests.
 *              The design pattern used here is Singleton. This allows us to have the same data in different windows. 
 *              Instead of passing all these data structures between the different window, we can just use the single instance of this class.
 * Date last modified: 08/12/2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757_Napier_Reservation_System
{
    public class DataSingleton
    {
        private Dictionary<int, Customer> allCustomers = new Dictionary<int, Customer>();
        private Dictionary<int, Booking> allBookings = new Dictionary<int, Booking>();
        private List<Guest> tempGuestList = new List<Guest>();
        private List<Extra> tempExtraList = new List<Extra>();
        private static DataSingleton instance;

        private DataSingleton() { }
        public static DataSingleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DataSingleton();
                }
                return instance;
            }
        }

        // Sets and gets the dictionary for all customers.
        public Dictionary<int,Customer>AllCustomers
        {
            get
            {
                return allCustomers;
            }
            set
            {
                foreach (Customer customer in value.Values)
                {
                    allCustomers.Add(customer.CustomerRefNumber, customer);
                }
            }
        }

        // Sets and gets the dictionary for all bookings.
        public Dictionary<int, Booking> AllBookings
        {
            get
            {
                return allBookings;
            }
            set
            {
                foreach (Booking booking in value.Values)
                {
                    allBookings.Add(booking.BookingRefNumber, booking);
                }
            }
        }

        // Gets the list of all guests.
        public List<Guest> TempGuestList
        {
            get
            {
                return tempGuestList;
            }
        }

        // Gets the list of all extras.
        public List<Extra> TempExtraList
        {
            get
            {
                return tempExtraList;
            }
        }

        // Adds a single extra to the temporary extra list.
        public void addExtra(Extra extra)
        {
            tempExtraList.Add(extra);
        }

        // Adds a single guest to the temporaty guest list.
        public void addGuest(Guest guest)
        {
            tempGuestList.Add(guest);
        }

        // Adds a single customer to the allCustomers dictionary.
        public void addCustomer(Customer customer)
        {
            allCustomers.Add(customer.CustomerRefNumber, customer);
        }

        // Adds a single booking to the allBookings dictionary.
        public void addBooking(Booking booking)
        {
            allBookings.Add(booking.BookingRefNumber, booking);
        }
    }
}
