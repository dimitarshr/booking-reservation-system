# README #

### What is this repository for? ###

The system is able to accept a variety of holiday bookings for customers. Each basic booking comprises the following attributes:

* Arrival Date 
* Departure Date
* Booking Reference Number

In addition each booking is associated with a customer who has the following attributes:

* Name
* Address
* Customer Reference Number

A customer may have many bookings, each booking must be associated with a customer. Each booking can have a number of guests,up to 
a maximum of 4 (a customer is not necessarily a guest):

* Name
* Passport Number 
* Age

The cost of a basic holiday is calculated as follows:

* Basic cost per night per person £50 (£30 if under 18)
* For each person the basic cost is multiplied by the number of nights booked
* The total cost the cost for each person, plus extras (see below)

Holidays may have a number of extra options added on

* Evening meals: A holiday may have evening meals added on to it, the cost is an extra £15 per person per night. 
* Breakfast: A holiday may have breakfast added on to it, the costs an extra £5 per person per day.
* Car hire: Car hire costs £50 per day extra. 

The system allows bookings to be added, amended and deleted. It can cope with any number of customers, each of whom may have multiple 
bookings. It is possible to amend a booking (including adding or removing extras) once it has been entered. The system is also able to 
produce invoices showing the cost of a booking. The invoice shows the costs per night and the costs of any extras.

# The project also contains Unit tests that can be run and observed. For more information, please refer to the *Class Diagram and non-GUI classes* PDF file and the *source code* #