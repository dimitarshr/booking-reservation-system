﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The BookingTests class consists of unit tests. 
 *              They are testing the behaviour of the Booking class and the necessary objects associated with each booking.
 * Date last modified: 09/12/2016
*/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using _40201757_Napier_Reservation_System;

namespace NapierReservationSystemTests
{
    [TestClass]
    public class BookingTests
    {
        [TestMethod]
        public void Correct_BookingID()
        {
            int expected = 2;
            // The first created booking has booking ID 1.
            DateTime fArrivalDate = new DateTime(2017,12,4);
            DateTime fDepartureDate = new DateTime(2017, 12, 22);
            Customer fCurrentCustomer = new Customer("Peter","Colinton Road");
            Booking fNewBooking = new Booking(fArrivalDate, fDepartureDate, fCurrentCustomer);
            
            // The second created booking is supposed to have booking ID 2.
            DateTime sArrivalDate = new DateTime(2017, 10, 4);
            DateTime sDepartureDate = new DateTime(2017, 10, 25);
            Customer sCurrentCustomer = new Customer("Ian", "Riego Street");
            Booking sNewBooking = new Booking(sArrivalDate, sDepartureDate, sCurrentCustomer);

            // Get the actual booking ID of the second booking by using the BookingRefNumber property.
            int actual = sNewBooking.BookingRefNumber;

            Assert.AreEqual(expected, actual, "Booking ID not working correctly!");
        }

        [TestMethod]
        public void Recreate_Booking()
        {
            int expected = 5;
            // When reading from a file I have to recreate the booking object.
            // In order to be consistent all existing booking should be recreated with their existing booking ID.
            int bookingReference = 5;
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("Peter", "Colinton Road");
            Booking newBooking = new Booking(bookingReference, arrivalDate, departureDate, currentCustomer);

            // Get the actual booking ID of the recreated booking by using the BookingRefNumber property.
            int actual = newBooking.BookingRefNumber;

            Assert.AreEqual(expected, actual, "Booking ID not working correctly!");
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void Booking_customer_with_no_name()
        {
            // Create booking.
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("", "Colinton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void Booking_customer_with_no_address()
        {
            // Create booking.
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("Peter", "");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Booking_with_arrivalDate_after_the_departureDate()
        {
            // We try to set the arrival date to be after the departure date.
            DateTime arrivalDate = new DateTime(2017, 12, 22);
            DateTime departureDate = new DateTime(2017, 12, 4);
            Customer currentCustomer = new Customer("Peter", "Colinton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Booking_with_arrivalDate_in_the_past()
        {
            // We try to set the arrival date to be in the past, compared with the current date.
            DateTime arrivalDate = new DateTime(2016, 12, 3);
            DateTime departureDate = new DateTime(2017, 12, 25);
            Customer currentCustomer = new Customer("Peter", "Colinton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Edit_Booking_with_departureDate_in_the_past()
        {
            // A booking is created with the correct data.
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("Peter", "Colinton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);

            // Edit the booking by setting the departure date in the past.
            newBooking.DepartureDate = new DateTime(2016, 12, 3);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Edit_booking_with_arrivalDate_in_the_past()
        {   
            // A booking is created with the correct data.
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("Peter", "Colinton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);
            
            // Edit the booking by setting the arrival date in the past.
            newBooking.ArrivalDate = new DateTime(2016,12,3);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Booking_with_more_than_four_guests()
        {
            int numberOfGuest = 5;
            List<Guest> guestList = new List<Guest>();
            // Create booking.
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("Peter", "Colinton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);
            // Create a list of 5 guests.
            for (int index = 0; index < numberOfGuest; index++)
            {
                Guest guest = new Guest("Peter","ARS123",20);
                guestList.Add(guest);
            }
            // Add the guest list to the current booking.
            newBooking.GuestList = guestList;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Booking_with_no_guests()
        {
            List<Guest> guestList = new List<Guest>();

            // Create booking.
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("Peter", "Colinton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);

            // Add the empty guest list to the current booking.
            newBooking.GuestList = guestList;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Booking_guest_with_no_name()
        {
            List<Guest> guestList = new List<Guest>();

            // Create booking.
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("Peter", "Colinton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);

            // Create guest object.
            Guest guest = new Guest("","ABS123",33);
            guestList.Add(guest);

            // Add the guest list to the current booking.
            newBooking.GuestList = guestList;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Booking_guest_with_negative_age()
        {
            List<Guest> guestList = new List<Guest>();

            // Create booking.
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("Peter", "Colionton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);

            // Create guest object.
            Guest guest = new Guest("Ian", "ABS123", -20);
            guestList.Add(guest);

            // Add the guest list to the current booking.
            newBooking.GuestList = guestList;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Booking_guest_with_age_above_101()
        {
            List<Guest> guestList = new List<Guest>();

            // Create booking.
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("Peter", "Colionton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);

            // Create guest object.
            Guest guest = new Guest("Ian", "ABS123", 120);
            guestList.Add(guest);

            // Add the guest list to the current booking.
            newBooking.GuestList = guestList;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Booking_guest_with_passportNumber_longer_than_10_characters()
        {
            List<Guest> guestList = new List<Guest>();

            // Create booking.
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("Peter", "Colionton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);

            // Create guest object.
            Guest guest = new Guest("Ian", "ABS1231233212131", 20);
            guestList.Add(guest);

            // Add the guest list to the current booking.
            newBooking.GuestList = guestList;
        }

        [TestMethod]
        public void Total_cost_booking()
        {
            int numberOfGuest = 4;
            List<Guest> guestList = new List<Guest>();
            List<Extra> extraList = new List<Extra>();
            // Create booking.
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("Peter", "Colionton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);
            // Create a list of 4 guests, all of them at age 20.
            for (int index = 0; index < numberOfGuest; index++)
            {
                Guest guest = new Guest("Peter", "ARS123", 20);
                guestList.Add(guest);
            }

            // Add the guest list to the current booking.
            newBooking.GuestList = guestList;

            // Add Breakfast extra.
            Meal fExtraMeal = new Meal();
            Breakfast extraBreakfast = new Breakfast(fExtraMeal);
            extraList.Add(extraBreakfast);
            // Add Evening Meal extra.
            Meal sExtraMeal = new Meal();
            EveningMeal extraEveningMeal = new EveningMeal(sExtraMeal);
            extraList.Add(extraEveningMeal);

            // Add the extra list to the current booking
            newBooking.AllExtra = extraList;

            // Calculation the expected and the actual amount of money.
            double nights = (departureDate - arrivalDate).TotalDays;
            double expected = ((numberOfGuest * 50) + (numberOfGuest * extraBreakfast.Price) + (numberOfGuest * extraEveningMeal.Price)) * nights;
            double actual = newBooking.totalBillBooking();

            Assert.AreEqual(expected, actual, "Booking total bill is not working correctly!");
        }

        [TestMethod]
        public void Cost_for_one_night_without_extras_booking()
        {
            int numberOfGuest = 4;
            List<Guest> guestList = new List<Guest>();
            // Create booking.
            DateTime arrivalDate = new DateTime(2017, 12, 4);
            DateTime departureDate = new DateTime(2017, 12, 22);
            Customer currentCustomer = new Customer("Peter", "Colionton Road");
            Booking newBooking = new Booking(arrivalDate, departureDate, currentCustomer);
            // Create a list of 4 guests, all at the age 20.
            for (int index = 0; index < numberOfGuest; index++)
            {
                Guest guest = new Guest("Peter", "ARS123", 20);
                guestList.Add(guest);
            }

            // Add the guest list to the current booking.
            newBooking.GuestList = guestList;

            // Calculation the expected and the actual amount of money.
            double nights = (departureDate - arrivalDate).TotalDays;
            double expected = (numberOfGuest * 50);
            double actual = newBooking.billBookingPerNight();

            Assert.AreEqual(expected, actual, "Booking total bill is not working correctly!");
        }
    }
}
